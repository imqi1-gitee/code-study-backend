#include <iostream>
#include <string>
#include <httplib.h>
using namespace std;

void handle_post(const httplib::Request req, httplib::Response res)
{
	string body = req.body;
	cout << "Request:" << body << endl;
	res.set_content("Post request", "application/json");
}

int main()
{
	httplib::Server ser;

	// 第一个参数为URL
	ser.Get("/", [](const httplib::Request& req, httplib::Response& res) {
		res.set_content("Test successfully!", "text/plain");
		});

	ser.Post("/about", handle_post);

	// 参数为IP和端口号
	ser.listen("localhost", 8080);
	return 0;
}